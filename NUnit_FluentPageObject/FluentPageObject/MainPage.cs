﻿using NUnit_FluentPageObject.Core_Singleton;
using System.Threading;

namespace NUnit_FluentPageObject.FluentPageObject
{
    public class MainPage : BaseFluentPageSingleton<MainPage, MainPageElementMap, MainPageValidator>
    {
        
        protected MainPage() { }
       
        public new MainPage Navigate(string url = @"http://www.google.com/")
        {
            base.Navigate(url);
            return this;
        }

        public MainPage Search(string textToType)
        {
            Map.SearchBox.Clear();
            Map.SearchBox.SendKeys(textToType);
            Map.GoButton.Click();
            return this;
        }

        public MainPage ClickImages()
        {
            Map.ImagesLink.Click();
            return this;
        }

        public MainPage CLickFirstImage()
        {
            Map.ImageFirst.Click();
            Thread.Sleep(1000);
            return this;
        }
      
        public MainPage GetTypeSpan()
        {
            Map.SpanFirst.GetType();
            Thread.Sleep(1000);
            return this;
        }      

    }
}
