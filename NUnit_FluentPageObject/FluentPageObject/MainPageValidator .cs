﻿using NUnit.Framework;
using NUnit_FluentPageObject.Core_Singleton;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_FluentPageObject.FluentPageObject
{
    public class MainPageValidator : BasePageValidator<MainPage, MainPageElementMap, MainPageValidator>
    {
        public MainPage ResultsCount(string expectedCount)
        {
            Assert.IsTrue(Map.ResultsCountDiv.Text.Contains(expectedCount));
            return PageInstance;
        }
    }
}
