﻿using NUnit_FluentPageObject.Core_Singleton;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace NUnit_FluentPageObject.FluentPageObject
{
     public class MainPageElementMap : BasePageElementMap
        {
            public IWebElement SearchBox
            {
                get
                {
                    return Browser.FindElement(By.Name("q"));
                }
            }

            public IWebElement GoButton
            {
                get
                {
                    return Browser.FindElement(By.ClassName("gNO89b"));
                }
            }

            public IWebElement ResultsCountDiv 
            {
                get
                {
                    return Browser.FindElement(By.ClassName("LC20lb"));
                }
            }

            public IWebElement ImagesLink
            {
                get
                {
                    return Browser.FindElement(By.LinkText("Зображення"));
                }
            }
        public IWebElement ImageFirst
        {
            get
            {

                return Browser.FindElement(By.Id("bAjKxLbdzqLfsM:"));
            }
        }
       
        public IWebElement SpanFirst
        {
            get
            {
                return Browser.FindElement(By.ClassName("dtviD"));
            }
        }              
     }
}
