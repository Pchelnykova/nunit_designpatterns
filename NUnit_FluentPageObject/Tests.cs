using NUnit.Framework;
using NUnit_FluentPageObject.Core_Singleton;
using NUnit_FluentPageObject.FluentPageObject;

namespace Tests
{
    [TestFixture]
    public class FluentSearchEngineTests
    {
        [SetUp]
        public void SetupTest()
        {
            Driver.StartBrowser();
        }

        [TearDown]
        public void TeardownTest()
        {
            Driver.StopBrowser();
            //Driver.Browser.PageSource
        }

        [Test]
        public void SearchForImageFuent()
        {
            MainPage
                                   .Instance
                                   .Navigate()
                                   .Search("Seaside")
                                   .ClickImages()
                                   .CLickFirstImage()                                   
                                   .GetTypeSpan();


            //MainPage mainPage = new MainPage();
            //mainPage.Navigate();
            //mainPage.Search("facebook");
            //mainPage.ClickImages();
            //mainPage.CLickFirstImage();
            //mainPage.GetTypeSpan();
            
        }
    }
}