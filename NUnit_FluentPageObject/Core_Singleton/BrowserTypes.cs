﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_FluentPageObject.Core_Singleton
{
    public enum BrowserTypes
    {
        Firefox,    
        InternetExplorer,
        Chrome      
    }
}
