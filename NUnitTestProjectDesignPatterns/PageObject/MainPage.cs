using NUnit.Framework;
using NUnitTestProjectDesignPatterns.PageObject;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace Tests
{
    public class MainPage
    {

        private readonly IWebDriver _browser;
        private readonly string _url = @"http://www.google.com/";

        public MainPage(IWebDriver browser)
        {
            _browser = browser;
        }

        protected MainPageElementMap Map
        {
            get
            {
                return new MainPageElementMap(_browser);
            }
        }

        public MainPageValidator Validate()
        {
            return new MainPageValidator(_browser);
        }

        public void Navigate()
        {
            _browser.Navigate().GoToUrl(_url);
        }

        public void Search(string textToType)
        {
            Map.SearchBox.Clear();
            Map.SearchBox.SendKeys(textToType);
            Map.GoButton.Click();
        }
    }
}