﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Reflection;

namespace NUnitTestProjectDesignPatterns.PageObject
{
    public class MainPageElementMap
    {
        private readonly IWebDriver _browser;
       
        public MainPageElementMap(IWebDriver browser)
        {
            _browser = browser;           
        }

        public IWebElement SearchBox
        {
            get
            {
                return _browser.FindElement(By.Name("q"));
            }
        }

        public IWebElement GoButton
        {
            get
            {
                
                return _browser.FindElement(By.ClassName("gNO89b"));
            }
        }

        public IWebElement ResultsCountDiv
        {
            get
            {

                return _browser.FindElement(By.ClassName("LC20lb"));
            }
        }
    }
}
