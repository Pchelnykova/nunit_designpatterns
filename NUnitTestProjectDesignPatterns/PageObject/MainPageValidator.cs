﻿using NUnit.Framework;
using OpenQA.Selenium;

using System;
using System.Collections.Generic;
using System.Text;

namespace NUnitTestProjectDesignPatterns.PageObject
{
    public class MainPageValidator
    {
        private readonly IWebDriver _browser;
       
        public MainPageValidator(IWebDriver browser)
        {
            _browser = browser;
        }

        protected MainPageElementMap Map
        {
            get
            {
                return new MainPageElementMap(_browser);
            }
        }

        public void ResultsCount(string expectedCount)
        {
      
            Assert.IsTrue(Map.ResultsCountDiv.Text.Contains(expectedCount), "The results DIV doesn't contains the specified text.");
        }
    }
}
