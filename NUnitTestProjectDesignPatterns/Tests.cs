﻿using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System;
using Tests;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;
using System.Reflection;
using System.Threading;


namespace NUnitTestProjectDesignPatterns
{
    [TestFixture]
    public class Tests 
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }
      


        [SetUp]
        public void SetupTest()
        {
            Driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            Wait = new WebDriverWait (  Driver, TimeSpan.FromSeconds(1000));

        }

        [TearDown]
        public void TeardownTest()
        {
            Driver.Quit();
        }

        [Test]
        public void SearchTextIn_First()
        {
            var mainPage = new MainPage(Driver);
            mainPage.Navigate();
            mainPage.Search("SoftServe");
            mainPage.Validate().ResultsCount("SoftServe");
        }
        

        [Test]
        public void SearchTextIn_Second()
        {
            var mainPage = new MainPage(Driver);
            mainPage.Navigate();          
            mainPage.Search("Automate The Planet");          
            mainPage.Validate().ResultsCount("Automate The Planet");        

        }
        [Test]
        public void SearchTextIn_Third()
        {
            var mainPage = new MainPage(Driver);
            mainPage.Navigate();
            mainPage.Search("Some unpopular site");
            mainPage.Validate().ResultsCount("some unpopular site");

        }


    }
}
