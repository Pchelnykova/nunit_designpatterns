using NUnit.Framework;
using NUnit_PageFactory.PageFactory;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace NUnit_PageFactory.PageObject
{
    public class MainPage : AbstractPageObject
    {

        private readonly IWebDriver _browser;
        private readonly string _url = @"http://www.google.com/";

        public MainPage(IWebDriver browser)
        {
            _browser = browser;
        }

        public MainPage()
        {
        }

        protected MainPageElementMap Map
        {
            get
            {
                return new MainPageElementMap(_browser);
            }
        }

        public override MainPageValidator Validate()
        {
            return new MainPageValidator(_browser);
        }

        public override void Navigate()
        {
            _browser.Navigate().GoToUrl(_url);
        }

        public override void Search(string textToType)
        {
            Map.SearchBox.Clear();
            Map.SearchBox.SendKeys(textToType);
            Map.GoButton.Click();
        }
    }
}