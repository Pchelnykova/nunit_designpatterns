﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit_PageFactory.Enums;

namespace NUnit_PageFactory.PageObject
{
    public abstract class AbstractPageObject
    {
        public abstract void Navigate();

        public abstract void Search(string textToType);

        public abstract MainPageValidator Validate();
    }
}
