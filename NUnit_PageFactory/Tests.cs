using NUnit.Framework;
using NUnit_PageFactory.PageFactory;
using NUnit_PageFactory.PageObject;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Reflection;

namespace NUnit_PageFactory
{
    [TestFixture]
    public class Tests
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [SetUp]
        public void SetupTest()
        {
            Driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            Wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30));
        }

        [TearDown]
        public void TeardownTest()
        {
            Driver.Quit();
        }

        [Test]
        public void SearchTextIn_First()
        {
            //var mainPage = new MainPage(Driver);
            var mainPage = new MainPageFactory().initInstance(Driver);
            mainPage.Navigate();
            mainPage.Search("SoftServe");
            mainPage.Validate().ResultsCount("SoftServe");
        }

        [Test]
        public void SearchTextIn_Second()
        {
            //var mainPage = new MainPage(Driver);
            var mainPage = new MainPageFactory().initInstance(Driver);
            mainPage.Navigate();
            mainPage.Search("Automate The Planet");
            mainPage.Validate().ResultsCount("Automate The Planet");
        }
       
    }
}