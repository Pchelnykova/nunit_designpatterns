﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit_PageFactory.Enums;
using NUnit_PageFactory.PageObject;
using OpenQA.Selenium;

namespace NUnit_PageFactory.PageFactory
{
   public abstract class PageFactoryAbstract
    {
        public abstract AbstractPageObject initInstance( IWebDriver webDriver);
       
    }
}
