﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit_PageFactory.PageObject;
using OpenQA.Selenium;

namespace NUnit_PageFactory.PageFactory
{
    public class MainPageFactory : PageFactoryAbstract
    {
        public override AbstractPageObject initInstance(IWebDriver webDriver)
        {
            return new MainPage(webDriver);
        }
    }
}
